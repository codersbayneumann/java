package coders.bay.institut.service;

import coders.bay.institut.dtos.PostTeacherDto;
import coders.bay.institut.dtos.TeacherDto;
import coders.bay.institut.entity.Teacher;
import coders.bay.institut.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    public TeacherDto addTeacher(PostTeacherDto postTeacherDto) {

        Teacher teacher = Teacher.builder()
                .name(postTeacherDto.getName())
                .age(postTeacherDto.getAge())
                .subject(postTeacherDto.getSubject())
                .build();

        teacherRepository.save(teacher);

        TeacherDto teacherDto = new TeacherDto(
                teacher.getTeacherId(),
                teacher.getName(),
                teacher.getAge(),
                teacher.getSubject());

        return teacherDto;
    }

    public TeacherDto showTeacherById(Long teacherId) {

        Optional<Teacher> teacherOptional = teacherRepository.findById(teacherId);

        if (teacherOptional.isEmpty()) {
            throw new NoSuchElementException("Es gibt keine Lehrperson mit der Id" + teacherId + "in der Datenbank gefunden!-");
        }

        Teacher teacher = teacherOptional.get();

        TeacherDto teacherDto = new TeacherDto(
                teacher.getTeacherId(),
                teacher.getName(),
                teacher.getAge(),
                teacher.getSubject()
        );
        return teacherDto;
    }

    public List<TeacherDto> showAllTeachers() {

        Iterable<Teacher> teacherIterable = teacherRepository.findAll();
        List<TeacherDto> teacherDtoList = new ArrayList<>();

        for (Teacher teacher : teacherIterable) {
            TeacherDto teacherDto = new TeacherDto(
                    teacher.getTeacherId(),
                    teacher.getName(),
                    teacher.getAge(),
                    teacher.getSubject());

            teacherDtoList.add(teacherDto);
        }
        return teacherDtoList;

    }
}
