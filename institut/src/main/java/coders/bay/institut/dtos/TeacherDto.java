package coders.bay.institut.dtos;

import coders.bay.institut.enums.Subject;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class TeacherDto {
    private Long teacherId;
    private String name;
    private int age;
    private Subject subject;
}
