package coders.bay.institut.dtos;

import coders.bay.institut.enums.Subject;
import lombok.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RequestPostPutTeacherDto {
    private String name;
    private int age;
    private Subject subject;
}
