package coders.bay.institut.controller;

import coders.bay.institut.dtos.PostTeacherDto;
import coders.bay.institut.dtos.TeacherDto;
import coders.bay.institut.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @RequestMapping("/api/teacher")

    @PostMapping
    public ResponseEntity<?> addTeacher(PostTeacherDto postTeacherDto){

        TeacherDto teacherDto;

        try {
            teacherDto = teacherService.addTeacher(postTeacherDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(teacherDto, HttpStatus.CREATED);
    }
}
