package spring.controllertask;

import lombok.Getter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import spring.controllertask.model.entity.Article;
import spring.controllertask.model.entity.User;

import java.util.HashSet;
import java.util.Set;

@Getter
@SpringBootApplication
public class ControllerTaskApplication {

    public static Set<User> userSet = new HashSet<>();
    public static Set<Article> articleSet = new HashSet<>();

    public ControllerTaskApplication() {
    }
    public ControllerTaskApplication(Set<User> userSet, Set<Article> articleSet) {
        ControllerTaskApplication.userSet = userSet;
        ControllerTaskApplication.articleSet = articleSet;
    }
    public ControllerTaskApplication(Set<Article> articleSet) {
        this.articleSet = articleSet;
    }

    public static void main(String[] args) {
        SpringApplication.run(ControllerTaskApplication.class, args);
    }


}
