package spring.controllertask.model.entity;

public class Article {

    private int articleID;
    private String articleName;
    private double articlePrice;

    public Article() {
    }

    public Article(int articleID, String articleName, double articlePrice) {
        this.articleID = articleID;
        this.articleName = articleName;
        this.articlePrice = articlePrice;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public void setArticlePrice(double articlePrice) {
        this.articlePrice = articlePrice;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getArticleName() {
        return articleName;
    }

    public double getArticlePrice() {
        return articlePrice;
    }


}
