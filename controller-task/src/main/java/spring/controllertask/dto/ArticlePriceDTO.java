package spring.controllertask.dto;

import lombok.*;

@Builder

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ArticlePriceDTO {
    private double articlePrice;


}
