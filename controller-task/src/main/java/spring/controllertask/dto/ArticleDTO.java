package spring.controllertask.dto;

public class ArticleDTO {
    private int articleID;
    private String articleName;
    private double articlePrice;

    public ArticleDTO() {
    }

    public ArticleDTO(int articleID, String articleName, double articlePrice) {
        this.articleID = articleID;
        this.articleName = articleName;
        this.articlePrice = articlePrice;
    }

    public int getArticleID() {
        return articleID;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public double getArticlePrice() {
        return articlePrice;
    }

    public void setArticlePrice(double articlePrice) {
        this.articlePrice = articlePrice;
    }
}
