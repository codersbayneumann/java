package spring.controllertask.dto;

public class UserDTO {
    private Long userId;
    private String username;
    private String password;
    private int age;

    public UserDTO() {
    }

    public UserDTO(Long userId, String username, String password, int age) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.age = age;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
