package spring.controllertask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.controllertask.ControllerTaskApplication;
import spring.controllertask.dto.ArticleDTO;
import spring.controllertask.dto.ArticlePriceDTO;
import spring.controllertask.model.entity.Article;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/article")
public class ArticleController {
//    @PostMapping
//    public String addArticle(@RequestBody ArticleDTO articleDTO) {
//
////        @Autowired
////        ArticleService articleService;
//
//        try {
//            articleService.addArticle(articleDTO);
//        } catch (Exception e) {
//
//            return "Fehler beim Anlegen";
//        }
//    }

    @GetMapping
    public List<ArticleDTO> getAllArticlesAsDTOs() {
        List<ArticleDTO> allArticlesDto = new ArrayList<>();

        for (Article article : ControllerTaskApplication.articleSet) {
            ArticleDTO newArticleDTO = new ArticleDTO(
                    article.getArticleID(),
                    article.getArticleName(),
                    article.getArticlePrice()
            );
            allArticlesDto.add(newArticleDTO);
        }
        return allArticlesDto;
    }

    @GetMapping("/{articleId}")
    public ResponseEntity<ArticleDTO> getArticleById(@PathVariable int articleId) {
        for (Article article : ControllerTaskApplication.articleSet) {
            if (article.getArticleID() == (articleId)) {
                ArticleDTO articleDTO = new ArticleDTO(
                        article.getArticleID(),
                        article.getArticleName(),
                        article.getArticlePrice()
                );
                return new ResponseEntity<>(articleDTO, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{articleId}")
    public ResponseEntity<ArticleDTO> updateArticlePrice(@PathVariable int articleId,
                                                         @RequestBody ArticlePriceDTO articlePriceDTO) {
        for (Article article : ControllerTaskApplication.articleSet) {
            if (article.getArticleID() == (articleId)) {

                article.setArticlePrice(articlePriceDTO.getArticlePrice());

                ArticleDTO articleDTO = new ArticleDTO(
                        article.getArticleID(),
                        article.getArticleName(),
                        article.getArticlePrice()
                );
                return new ResponseEntity<>(articleDTO, HttpStatus.CREATED);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
