package spring.controllertask.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.controllertask.dto.NumberDTO;

@RestController
@RequestMapping("api/test")
public class TestController {

    @GetMapping
    public String firstGetRequest() {
        return "Mein erster Request";
    }

    @PostMapping()
    public boolean checkNumber(@RequestBody NumberDTO number) {
        return number.getNumber() % 2 == 0;
    }

    @GetMapping("{input}")
    public ResponseEntity<String> processString(@PathVariable String input) {
        if (input.length() < 3) {
            return ResponseEntity.badRequest().body("Bitte geben Sie mindestens drei Buchstaben ein.");
        }
        return ResponseEntity.ok(input.substring(3));
    }
}
