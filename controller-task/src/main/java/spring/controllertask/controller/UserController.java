package spring.controllertask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.controllertask.ControllerTaskApplication;
import spring.controllertask.dto.UserDTO;
import spring.controllertask.model.entity.User;

import spring.controllertask.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/user")

public class UserController {

    @Autowired
    UserService userService;

    @PostMapping()
    public String userCreation(@RequestBody UserDTO userDTO) {
        for (User user : ControllerTaskApplication.userSet) {
            if (user.getUserId().equals(userDTO.getUserId())) {
                return "User bereits vorhanden";
            }
        }
        User user = new User(userDTO.getUserId(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getAge());

        ControllerTaskApplication.userSet.add(user);
        return "User erfolgreich angelegt";
    }

    @DeleteMapping("/{userId}")
    public List<UserDTO> deleteUserById(@PathVariable int userId) {
        for (User user : ControllerTaskApplication.userSet) {
            if (userId == user.getUserId()) {
                ControllerTaskApplication.userSet.remove(user);
            }
        }
        List<UserDTO> userDtoList = new ArrayList<>();
        for (User user : ControllerTaskApplication.userSet) {
            UserDTO userDTO = new UserDTO(user.getUserId(),
                                          user.getUsername(),
                                          user.getPassword(),
                                          user.getAge());
            userDtoList.add(userDTO);
        }
        return userDtoList;
    }
}

