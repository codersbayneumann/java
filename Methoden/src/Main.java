// Aufgabe 5
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Aufgabe 1 & 2
        int a = 1;
        int b = 1;

        // Aufgabe 2
        int rows = 10;

        // Aufgabe 3
        double length = 13.35;
        double width = 15.22;
        double height = 8.75;

        // Aufgabe 4
        int primeCheckNum = 101;

        // Aufgabe 1
        addition(a, b);
        multiplication(a, b);
        division(a, b);
        maximumValue(a, b);
        averageValue(a, b);
        System.out.println(additionReturn(a, b));
        System.out.println(multiplicationReturn(a, b));
        System.out.println(divisionReturn(a, b));
        System.out.println(maximumValueReturn(a, b));
        System.out.println(averageValueReturn(a, b));

        // Aufgabe 2
        numericalRows(rows);

        // Aufgabe 3
        cuboidCalculations(length, width, height);

        // Aufgabe 4
        primeNumberCheck(primeCheckNum);

        // Aufgabe 5
        diceRolls();
    }

    // Aufgabe 1
    public static void addition(double a, double b) {
        System.out.println(a + b);
    }

    public static void multiplication(double a, double b) {
        System.out.println(a * b);
    }

    public static void division(double a, double b) {
        System.out.println(a / b);
    }

    public static void maximumValue(double a, double b) {
        System.out.println(Math.max(a, b));
    }

    public static void averageValue(double a, double b) {
        System.out.println((a + b) / 2);
    }

    public static double additionReturn(double a, double b) {
        return a + b;
    }

    public static double multiplicationReturn(double a, double b) {
        return a * b;
    }

    public static double divisionReturn(double a, double b) {
        return a / b;
    }

    public static double maximumValueReturn(double a, double b) {
        return Math.max(a, b);
    }

    public static double averageValueReturn(double a, double b) {
        return (a + b) / 2;
    }

    // Aufgabe 2
    public static void numericalRows(int rows) {
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= rows; j++) {
                System.out.println(j + " x " + i + " = " + j * i);
            }
        }
    }

    // Aufgabe 3
    public static void cuboidCalculations(double length, double width, double height) {
        double cuboidVolumina = length * width * height;
        double cuboidSurface = 2 * (length * height + width * height + length * width);
        double cuboidSpaceDiagonal = Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2) + Math.pow(height, 2));
        System.out.println("Volumen des Quaders: " + cuboidVolumina +
                "\nOberfläche des Quaders: " + cuboidSurface + "\nRaumdiagonale des Quaders: " + cuboidSpaceDiagonal);
    }

    // Aufgabe 4
    public static void primeNumberCheck(int primeCheckNum) {
        if ((primeCheckNum % 2 == 0) || (primeCheckNum % 3 == 0) || (primeCheckNum % 5 == 0) || (primeCheckNum % 7 == 0)) {
            System.out.println(primeCheckNum + " ist keine Primzahl.");
        } else {
            System.out.println(primeCheckNum + " ist eine Primzahl.");
        }
    }

    // Aufgabe 5
    public static void diceRolls() {
        Scanner sca = new Scanner(System.in);
        Random random = new Random();

        int diceSides = 6;

        System.out.println("Wie viele Würfel sollen wir für dich rollen lassen? ");
        int numDices = sca.nextInt();

        for (int i = 0; i < numDices; i++) {
            int rollResult = random.nextInt(diceSides) + 1;
            System.out.println("Wurf ergibt " + rollResult);
        }
    }
}