package kinoprojekt.controller;

import kinoprojekt.dto.CinemaDto;
import kinoprojekt.dto.PostCinemaDto;
import kinoprojekt.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/cinema")
public class CinemaController {

    @Autowired
    CinemaService cinemaService;
    @PostMapping
    public ResponseEntity<?> addCinema(@RequestBody PostCinemaDto postCinemaDto){

        CinemaDto cinemaDto;

        try{
            cinemaDto = cinemaService.addCinema(postCinemaDto);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(cinemaDto, HttpStatus.CREATED);
    }
    @GetMapping("{cinemaId}")
    public ResponseEntity<?> showCinemaById(@PathVariable Long cinemaId){

        CinemaDto cinemaDto;

        try {
            cinemaDto = cinemaService.showCinemaById(cinemaId);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cinemaDto, HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<?> showAllCinemas(){

        List<CinemaDto> cinemaDtoList;
        try{
            cinemaDtoList = cinemaService.showAllCinemas();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(cinemaDtoList, HttpStatus.OK);
    }
    @DeleteMapping("/{cinemaId}")
    public ResponseEntity<?> deleteCinemaById(@PathVariable Long cinemaId){

        try {
            return cinemaService.deleteCinemaById(cinemaId);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
