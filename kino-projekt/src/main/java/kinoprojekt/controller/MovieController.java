package kinoprojekt.controller;

import kinoprojekt.dto.MovieDto;
import kinoprojekt.dto.RequestMovieDto;
import kinoprojekt.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/movie")
public class MovieController {

    @Autowired
    MovieService movieService;

    @PostMapping
    public ResponseEntity<?> addMovie(@RequestBody RequestMovieDto postMovieDto) {

        MovieDto movieDto;

        try {
            movieDto = movieService.addMovie(postMovieDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(movieDto, HttpStatus.CREATED);
    }

    @PutMapping("{movieId}")
    ResponseEntity<?> editMovie(@RequestBody RequestMovieDto postMovieDto, @PathVariable Long movieId) {
        MovieDto movieDto;

        try {
            movieDto = movieService.editMovie(postMovieDto, movieId);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(movieDto, HttpStatus.CREATED);
    }
}
