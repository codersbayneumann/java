package kinoprojekt.controller;

import kinoprojekt.dto.HallDto;
import kinoprojekt.dto.PostHallDto;
import kinoprojekt.dto.PutHallDto;
import kinoprojekt.service.HallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hall")
public class HallController {

    @Autowired
    HallService hallService;

    @PostMapping
    public ResponseEntity<?> addHall(@RequestBody PostHallDto postHallDto) {

        HallDto hallDto;

        try {
            hallDto = hallService.addHall(postHallDto);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(hallDto, HttpStatus.CREATED);
    }

    @GetMapping("/{hallId}")
    public ResponseEntity<?> showHallById(@PathVariable Long hallId) {

        HallDto hallDto;

        try {
            hallDto = hallService.showHallById(hallId);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(hallDto, HttpStatus.OK);
    }

    @PutMapping("{hallId}")
    ResponseEntity<?> editHall(@RequestBody PutHallDto putHallDto, @PathVariable Long hallId){

        HallDto hallDto;

        try {
            hallDto = hallService.editHall(putHallDto, hallId);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(hallDto, HttpStatus.CREATED);
    }

}
