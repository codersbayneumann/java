package kinoprojekt.dto;

import kinoprojekt.enums.MovieVersion;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RequestMovieDto {
    private String title;
    private String mainCharacter;
    private String description;
    private String premieredAt;
    private MovieVersion movieVersion;
    private Long hallId;
}
