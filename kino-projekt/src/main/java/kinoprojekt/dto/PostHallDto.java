package kinoprojekt.dto;

import kinoprojekt.enums.MovieVersion;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class PostHallDto {

    private int capacity;
    private int occupiedSeats;
    private MovieVersion supportedMovieVersion;
    private Long cinemaId;

}
