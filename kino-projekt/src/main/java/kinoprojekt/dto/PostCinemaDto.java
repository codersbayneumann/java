package kinoprojekt.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class PostCinemaDto {

    private String name;
    private String address;
    private String manager;
    private int maxHalls;

}
