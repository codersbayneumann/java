package kinoprojekt.dto;

import kinoprojekt.enums.MovieVersion;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class HallDto {

    private Long hallId;
    private int capacity;
    private int occupiedSeats;
    private MovieVersion supportedMovieVersion;
    private Long cinemaId;
}
