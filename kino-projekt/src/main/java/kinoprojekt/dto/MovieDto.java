package kinoprojekt.dto;

import kinoprojekt.entity.Hall;
import kinoprojekt.enums.MovieVersion;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MovieDto {

    private Long movieId;
    private String title;
    private String mainCharacter;
    private String description;
    private String premieredAt;
    private MovieVersion movieVersion;
    private HallDto hallDto;

}
