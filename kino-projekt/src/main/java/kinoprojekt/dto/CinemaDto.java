package kinoprojekt.dto;

import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class CinemaDto {

    private Long cinemaId;
    private String name;
    private String address;
    private String manager;
    private int maxHalls;
    private List<HallDto> hallList;

}
