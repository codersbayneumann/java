package kinoprojekt.service;

import kinoprojekt.dto.HallDto;
import kinoprojekt.dto.MovieDto;
import kinoprojekt.dto.RequestMovieDto;
import kinoprojekt.entity.Hall;
import kinoprojekt.entity.Movie;
import kinoprojekt.repository.HallRepository;
import kinoprojekt.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    HallRepository hallRepository;

    public MovieDto addMovie(RequestMovieDto postMovieDto) {

        Optional<Hall> hallOptional = hallRepository.findById(postMovieDto.getHallId());
        if (hallOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Saal mit der ID" + postMovieDto.getHallId() + "in der Datenbank gefunden!");
        }
        Hall hall = hallOptional.get();

        if (hall.getSupportedMovieVersion() != postMovieDto.getMovieVersion()){
            throw new RuntimeException("Die gesendete Filmversion wird vom angegebenen Saal nicht unterstützt!");
        }

        Movie movie = Movie.builder()
                .movieVersion(postMovieDto.getMovieVersion())
                .title(postMovieDto.getTitle())
                .mainCharacter(postMovieDto.getMainCharacter())
                .description(postMovieDto.getDescription())
                .premieredAt(postMovieDto.getPremieredAt())
                .movieVersion(postMovieDto.getMovieVersion())
                .hall(hall)
                .build();

        movieRepository.save(movie);

        hall.getMovieList().add(movie);
        hallRepository.save(hall);

        HallDto hallDto = new HallDto(
                hall.getHallId(),
                hall.getCapacity(),
                hall.getOccupiedSeats(),
                hall.getSupportedMovieVersion(),
                hall.getCinema().getCinemaId()
        );

        MovieDto movieDto = new MovieDto(
                movie.getMovieId(),
                movie.getTitle(),
                movie.getMainCharacter(),
                movie.getDescription(),
                movie.getPremieredAt(),
                movie.getMovieVersion(),
                hallDto
        );
        return movieDto;
    }

    public MovieDto editMovie(RequestMovieDto postMovieDto, Long movieId) {

        Optional<Movie> movieOptional = movieRepository.findById(movieId);
        Optional<Hall> hallOptional = hallRepository.findById(postMovieDto.getHallId());

        if (movieOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Film mit der Id" + movieId + "in der Datenbank gefunden!");
        }
        if (hallOptional.isEmpty()) {
            throw new NoSuchElementException("Keine Halle mit der Id mit der angefragten ID in der Datenbank gefunden!");
        }
        Movie movie = movieOptional.get();
        Hall hall = hallOptional.get();

        if (hall.getSupportedMovieVersion() != postMovieDto.getMovieVersion()){
            throw new RuntimeException("Die geänderte Filmversion wird vom angegebenen Saal nicht unterstützt!");
        }

        movie.setTitle(postMovieDto.getTitle());
        movie.setMainCharacter(postMovieDto.getMainCharacter());
        movie.setDescription(postMovieDto.getDescription());
        movie.setPremieredAt(postMovieDto.getPremieredAt());
        movie.setMovieVersion(postMovieDto.getMovieVersion());
        movie.setHall(hallOptional.get());

        movieRepository.save(movie);


        if(!hall.getMovieList().contains(movie)){
            hall.getMovieList().add(movie);
            hallRepository.save(hall);
        }

        HallDto hallDto = new HallDto(
                movie.getHall().getHallId(),
                movie.getHall().getCapacity(),
                movie.getHall().getOccupiedSeats(),
                movie.getHall().getSupportedMovieVersion(),
                movie.getHall().getCinema().getCinemaId()
        );

        MovieDto movieDto = new MovieDto(
                movie.getMovieId(),
                movie.getTitle(),
                movie.getMainCharacter(),
                movie.getDescription(),
                movie.getPremieredAt(),
                movie.getMovieVersion(),
                hallDto
        );
        return movieDto;
    }
}
