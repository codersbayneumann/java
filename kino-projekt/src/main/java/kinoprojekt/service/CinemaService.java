package kinoprojekt.service;

import kinoprojekt.dto.CinemaDto;
import kinoprojekt.dto.HallDto;
import kinoprojekt.dto.PostCinemaDto;
import kinoprojekt.entity.Cinema;
import kinoprojekt.entity.Hall;
import kinoprojekt.repository.CinemaRepository;
import kinoprojekt.repository.HallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CinemaService {
    @Autowired
    CinemaRepository cinemaRepository;
    @Autowired
    HallRepository hallRepository;

    public CinemaDto addCinema(PostCinemaDto postCinemaDto) {

        List<Hall> hallList = new ArrayList<>();

        Cinema cinema = Cinema.builder()
                .name(postCinemaDto.getName())
                .address(postCinemaDto.getAddress())
                .manager(postCinemaDto.getManager())
                .maxHalls(postCinemaDto.getMaxHalls())
                .hallList(hallList)
                .build();

        cinemaRepository.save(cinema);

        List<HallDto> hallDtoList = new ArrayList<>();
        CinemaDto cinemaDto = new CinemaDto(
                cinema.getCinemaId(),
                cinema.getName(),
                cinema.getAddress(),
                cinema.getManager(),
                cinema.getMaxHalls(),
                hallDtoList);

        return cinemaDto;
    }

    public CinemaDto showCinemaById(Long cinemaId) {

        Optional<Cinema> cinemaOptional = cinemaRepository.findById(cinemaId);

        if (cinemaOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Kino mit der ID " + cinemaId + " in der Datenbank gefunden!");
        }
        Cinema cinema = cinemaOptional.get();

        List<HallDto> hallDtoList = new ArrayList<>();

        for (Hall hall : cinema.getHallList()) {

            HallDto hallDto = new HallDto(hall.getHallId(),
                    hall.getCapacity(),
                    hall.getOccupiedSeats(),
                    hall.getSupportedMovieVersion(),
                    hall.getCinema().getCinemaId());
            hallDtoList.add(hallDto);

        }

        CinemaDto cinemaDto = new CinemaDto(
                cinema.getCinemaId(),
                cinema.getName(),
                cinema.getAddress(),
                cinema.getManager(),
                cinema.getMaxHalls(),
                hallDtoList
        );
        return cinemaDto;
    }

    public List<CinemaDto> showAllCinemas() {

        Iterable<Cinema> cinemaIterable = cinemaRepository.findAll();

        List<CinemaDto> cinemaDtoList = new ArrayList<>();

        for (Cinema cinema : cinemaIterable) {

            List<HallDto> hallDtoList = new ArrayList<>();

            for (Hall hall : cinema.getHallList()) {

                HallDto hallDto = new HallDto(hall.getHallId(),
                        hall.getCapacity(),
                        hall.getOccupiedSeats(),
                        hall.getSupportedMovieVersion(),
                        hall.getCinema().getCinemaId());
                hallDtoList.add(hallDto);
            }

            CinemaDto cinemaDto = new CinemaDto(
                    cinema.getCinemaId(),
                    cinema.getName(),
                    cinema.getAddress(),
                    cinema.getManager(),
                    cinema.getMaxHalls(),
                    hallDtoList
            );
            cinemaDtoList.add(cinemaDto);
        }
        return cinemaDtoList;
    }

    public ResponseEntity<?> deleteCinemaById(Long cinemaId) {

        if (!cinemaRepository.existsById(cinemaId)) {
            throw new NoSuchElementException("Kein Kino mit der ID " + cinemaId + " in der Datenbank gefunden!");
        }
        Optional<Cinema> cinemaOptional = cinemaRepository.findById(cinemaId);

        Cinema cinema = cinemaOptional.get();
        for(Hall hall : cinema.getHallList()){

            if(hall.getMovieList().isEmpty()){
                hallRepository.deleteById(hall.getHallId());
            } else {
                throw new RuntimeException("Das Kino kann nicht gelöscht werden, weil es Filme zugeordnet hat!");
            }
        }

        cinemaRepository.deleteById(cinemaId);
        return new ResponseEntity<>("Kino mit der ID " + cinemaId + " wurde aus der Datenbank gelöscht!", HttpStatus.OK);



    }
}

