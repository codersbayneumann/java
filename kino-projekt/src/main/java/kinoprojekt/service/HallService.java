package kinoprojekt.service;

import kinoprojekt.dto.HallDto;
import kinoprojekt.dto.PostHallDto;
import kinoprojekt.dto.PutHallDto;
import kinoprojekt.entity.Cinema;
import kinoprojekt.entity.Hall;
import kinoprojekt.enums.MovieVersion;
import kinoprojekt.repository.CinemaRepository;
import kinoprojekt.repository.HallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class HallService {
    @Autowired
    HallRepository hallRepository;
    @Autowired
    CinemaRepository cinemaRepository;

    public HallDto addHall(@RequestBody PostHallDto postHallDto) {

        Optional<Cinema> cinemaOptional = cinemaRepository.findById(postHallDto.getCinemaId());
        if (cinemaOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Kino mit der ID " + postHallDto.getCinemaId() + " in der Datenbank gefunden!");
        }
        Cinema cinema = cinemaOptional.get();

        if (cinema.getHallList().size() >= cinema.getMaxHalls()) {
            throw new RuntimeException("Das Kino hat keinen zusätzlichen Platz für die angelegte Hall");
        }

        Hall hall = Hall.builder()
                .capacity(postHallDto.getCapacity())
                .occupiedSeats(postHallDto.getOccupiedSeats())
                .supportedMovieVersion(postHallDto.getSupportedMovieVersion())
                .cinema(cinema)
                .build();

        hallRepository.save(hall);

        cinema.getHallList().add(hall);
        cinemaRepository.save(cinema);

        HallDto hallDto = new HallDto(
                hall.getHallId(),
                hall.getCapacity(),
                hall.getOccupiedSeats(),
                hall.getSupportedMovieVersion(),
                hall.getCinema().getCinemaId());
        return hallDto;
    }

    public HallDto showHallById(Long hallId) {

        Optional<Hall> hallOptional = hallRepository.findById(hallId);

        if (hallOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Saal mit der ID " + hallId + " in der Datenbank gefunden!");
        }
        Hall hall = hallOptional.get();

        HallDto hallDto = new HallDto(
                hall.getHallId(),
                hall.getCapacity(),
                hall.getOccupiedSeats(),
                hall.getSupportedMovieVersion(),
                hall.getCinema().getCinemaId());
        return hallDto;
    }

    public HallDto editHall(PutHallDto putHallDto, Long hallId) {

        Optional<Hall> hallOptional = hallRepository.findById(hallId);
        if (hallOptional.isEmpty()) {
            throw new NoSuchElementException("Kein Saal mit der Id" + hallId + "in der Datenbank gefunden!");
        }
        Hall hall = hallOptional.get();

        if (!hall.getMovieList().isEmpty()) {
            throw new RuntimeException("Der Saal kann nicht gelöscht werden, da ein oder mehrere Filme zugeordnet sind!");
        }
        if (hall.getSupportedMovieVersion() == MovieVersion.valueOf("DBOX") && putHallDto.getSupportedMovieVersion() == MovieVersion.valueOf("R3D")) {

            hall.setCapacity(putHallDto.getCapacity());
            hall.setOccupiedSeats(putHallDto.getOccupiedSeats());
            hall.setSupportedMovieVersion(putHallDto.getSupportedMovieVersion());

            hallRepository.save(hall);

            HallDto hallDto = new HallDto(
                    hall.getHallId(),
                    hall.getCapacity(),
                    hall.getOccupiedSeats(),
                    hall.getSupportedMovieVersion(),
                    hall.getCinema().getCinemaId()
            );
            return hallDto;
        } else {
            throw new RuntimeException("Filmversionen dürfen nur von DBOX auf R3D verändert werden");
        }
    }
}

