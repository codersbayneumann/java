package kinoprojekt.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cinemaId;
    private String name;
    private String address;
    private String manager;
    private int maxHalls;
    @OneToMany(mappedBy = "cinema")
    private List<Hall> hallList;
}
