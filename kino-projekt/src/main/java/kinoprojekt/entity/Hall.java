package kinoprojekt.entity;

import jakarta.persistence.*;
import kinoprojekt.enums.MovieVersion;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity

public class Hall {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long hallId;
    private int capacity;
    private int occupiedSeats;
    private MovieVersion supportedMovieVersion;
    @ManyToOne
    @JoinColumn(name = "hallList")
    private Cinema cinema;
    @OneToMany(mappedBy = "hall")
    private List<Movie> movieList;
}
