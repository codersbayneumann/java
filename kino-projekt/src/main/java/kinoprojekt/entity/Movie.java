package kinoprojekt.entity;

import jakarta.persistence.*;
import kinoprojekt.enums.MovieVersion;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long movieId;
    private String title;
    private String mainCharacter;
    private String description;
    private String premieredAt;
    private MovieVersion movieVersion;
    @ManyToOne
    @JoinColumn(name = "hallId")
    private Hall hall;


}
