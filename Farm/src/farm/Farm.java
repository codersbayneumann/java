package farm;

import animals.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Farm {
    public List<Animal> animalList = new ArrayList<>();
    public List<Animal> animalSelectedList = new ArrayList<>();
    public Scanner sca;
    public String animalChoice;
    public String customAnimalKind;
    public String customAnimalName;
    static String feedingIntend;
    public Sheep sheep;
    private Cow cow;
    private Chicken chicken;

    public Farm() {
    }
    public Farm(Chicken chicken, Cow cow, Sheep sheep) {
        this.chicken = chicken;
        this.cow = cow;
        this.sheep = sheep;
    }

    // Add one or several animals to the list of all existing animals
    public void addToAnimalList(Animal nextAnimal) {
        animalList.add(nextAnimal);
    }

    // Add one or several animals to the list of all in the current round selected animals
    public void addToSelection(Animal selectedAnimal) {
        animalSelectedList.add(selectedAnimal);
    }

    // Remove one or several animals from the list of selected animals
    public void removeFromSelection() {
        animalSelectedList.clear();
    }

    public void startingDecision() {
        // Starting messages of the game.
        System.out.println("Es gibt schon mindestens ein Tier der Arten Schaf, Kuh und Huhn auf der Farm.");
        // Decision if new animal should be created or animal(s) from already existing pool should be used
        System.out.println("Möchtest du eines davon auswählen oder ein neues Tier erstellen?\nBitte 'play' oder 'add' hineintippen");
        sca = new Scanner(System.in);
        String decision = sca.nextLine().toLowerCase().trim();

        switch (decision) {
            // "add" starts the function for creating a new animal.
            case "add" -> addAnimal(decision);
            case "play" -> System.out.println("Okay, dann geht's los!");
            // Default case restarts the function
            default -> {
                System.out.println("Bitte entscheide dich für 'add' oder 'play'!");
                startingDecision();
            }
        }
    }
//    public void harvest() {
//        for (Animal animal : animalList) {
//            if (sheep.getWool() > 5)
//                System.out.println("Mindestens eines deiner Schafe wird geschoren, du erhältst Wolle!");
//        }
//    }
    public void addAnimal(String decision) {
        System.out.println("Welche Art soll dein Tier haben?");
        sca = new Scanner(System.in);
        customAnimalKind = sca.nextLine().toLowerCase().trim();
        // animalValidation function checks if input matches one of the existing animal classes.
        animalKindValidation(customAnimalKind);
        System.out.println("Welcher Name soll dein Tier zieren?");
        sca = new Scanner(System.in);
        customAnimalName = sca.nextLine().trim();
        // delivering the custom animal name for animal creation
        customAnimalCreation(customAnimalName);
    }

    // Function for creating animals, merging custom names with standard values.
    public void customAnimalCreation(String customAnimalName) {
        switch (customAnimalKind) {
            case "schaf" -> {
                Sheep customSheep = new Sheep(customAnimalName, 50, 5, 2);
                addToAnimalList(customSheep);
            }
            case "kuh" -> {
                Cow customCow = new Cow(customAnimalName, 60, 7, 3);
                addToAnimalList(customCow);
            }
            case "huhn" -> {
                Chicken customChicken = new Chicken(customAnimalName, 60, 7, 3);
                addToAnimalList(customChicken);
            }
        }
    }

    // Selection expecting a correct animal name
    public void animalChoice() {
        System.out.println("Welches Tier möchtest du diese Runde auswählen?");
        sca = new Scanner(System.in);
        animalChoice = sca.nextLine().toLowerCase().trim();
        // Adding animal to list of selected animals
        selectAnimal(animalChoice);
    }

    // Console print for each existing animal
    public void printHunger() {
        System.out.println("Folgende Tiere sind auf deiner Farm: ");
        for (Animal animal : animalList) {
            animal.print();
        }
    }

    public void printNames() {
        System.out.println("Deine Tierauswahl für diese Runde: ");
        for (Animal animal : animalSelectedList) {
            System.out.println(animal.getName());
        }
    }

    // Check if a correct animal type was input
    public void animalKindValidation(String animalKind) {
        switch (customAnimalKind) {
            case "schaf" -> System.out.println("Mäh das Schaf steht bereit.");
            case "kuh" -> System.out.println("Muh sagt die neue Kuh sofort und scheint motiviert.");
            case "huhn" -> System.out.println("Gok-gok gackert ein frisches Huhn dir entgegen und steht bereit.");
            default -> {
                System.out.println("Ich bitte Sie, wählen Sie 'Schaf', 'Kuh', oder 'Huhn'!");
                animalChoice();
            }
        }
    }

    public void selectAnimal(String animalChoice) {
        for (Animal nameCheckAnimal : animalList) {
            // If a correct name was input, the program tells the player that the animal is ready and adds it to the selectedAnimalsList
            if (animalChoice.equals(nameCheckAnimal.getName().toLowerCase().trim())) {
                addToSelection(nameCheckAnimal);
                System.out.println("Du hast " + nameCheckAnimal.getName() + " gewählt! TOLL!");
            }
        }
    }
    public void additionalAnimal() {
        String anotherAnimal;
        System.out.println("Möchtest du noch ein zweites Tier auswählen?");
        sca = new Scanner(System.in);
        anotherAnimal = sca.nextLine().toLowerCase().trim();
        if (anotherAnimal.equals("ja")) {
            animalChoice();
        }
    }
    public void hungerGain() {
        for (Animal animal : animalList) {
            animal.hunger();
        }
    }

    /** feedingDecision iterates through the selectedAnimalList and checks the hunger values of the animals
     *  If an animal is at a value of 75 or higher in hunger it gets fed automatically
     *  Between 50 and 74 value of hunger the player can decide if the animal should be fed
     *  Selected animals with hunger below 50 will be fed automatically
     *  - a do while loop is used to continue looping as long as the player does not input a valid answer.
     */
    public void feedingDecision(int hunger) {
        for (Animal animal : animalSelectedList) {
            if (animal.getHunger() >= 75) {
                System.out.println("Das Tier wird gefüttert");
                animal.feed(HungerManager.food);
            } else if (animal.getHunger() >= 50) {
                do {
                    System.out.println("Möchtest du dein Tier füttern?");
                    feedingIntend = sca.nextLine().toLowerCase().trim();
                    switch (feedingIntend) {
                        case "ja" -> {
                            System.out.println("Dein Tier wird gefüttert");
                            animal.feed(HungerManager.food);
                        }
                        case "nein" -> {
                        }
                        default -> System.out.println("Antworte mit 'Ja' oder 'Nein'");
                    }
                } while (!feedingIntend.equals("ja") && !feedingIntend.equals("nein"));
            } else {
                System.out.println("Das Tier kann nicht gefüttert werden");
            }
        }
    }

    public Chicken getChicken() {
        return chicken;
    }

    public void setChicken(Chicken chicken) {
        this.chicken = chicken;
    }

    public Cow getCow() {
        return cow;
    }

    public void setCow(Cow cow) {
        this.cow = cow;
    }
}


