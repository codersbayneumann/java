package animals;

public class Cow extends Animal {

    private int milk;

    public Cow(String name, int hunger, int age, int milk) {
        super(name, hunger, age);
        this.milk = milk;
    }

    @Override
    public void feed(int food) {
        hunger -= food / 2;
        milk++;
    }

    @Override
    public void print() {
        System.out.println("Eine Kuh mit dem Namen " + getName() + "\nHungerwert: " + getHunger());
    }

    public void hunger(int appetite) {
        hunger += appetite;
    }

    public void hungerReset() {
        hunger = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }
}
