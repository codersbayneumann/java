package animals;

public interface HungerManager {
    // Global gain of hunger value each round
    int hungerValue = 10;
    // Value used to reduce hunger when fed
    int food = 30;
    void hunger();
    void feed(int food);
}