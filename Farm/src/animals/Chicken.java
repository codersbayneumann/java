package animals;

public class Chicken extends Animal{

    private int eggs;

    public Chicken(String name, int hunger, int age, int eggs) {
        super(name, hunger, age);
        this.eggs = eggs;
    }
    @Override
    public void feed(int food){
        hunger -= food;
    }
    @Override
    public void print() {
        System.out.println("Ein Huhn mit dem Namen " + getName() +"\nHungerwert: " + getHunger());
    }
    public void hunger(int appetite){
        hunger += appetite;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getEggs() {
        return eggs;
    }

    public void setEggs(int eggs) {
        this.eggs = eggs;
    }
}
