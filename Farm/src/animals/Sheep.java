package animals;

public class Sheep extends Animal {

    private int wool;

    public Sheep(String name, int hunger, int age, int wool) {
        super(name, hunger, age);
        this.wool = wool;
    }

    @Override
    public void feed(int food) {
        super.feed(food);
        wool += 1;
    }

    @Override
    public void print() {
        System.out.println("Ein Schaf mit dem Namen " + getName() + "\nHungerwert: " + getHunger());
    }

    public void hunger(int appetite) {
        hunger += appetite;
    }

    public void hungerReset() {
        hunger = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWool() {
        return wool;
    }

    public void setWool(int wool) {
        this.wool = wool;
    }
}
