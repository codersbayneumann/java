package animals;

public abstract class Animal implements HungerManager {
    protected String name;
    protected int hunger;
    protected int age;


    public Animal(String name, int hunger, int age) {
        this.name = name;
        this.hunger = hunger;
        this.age = age;
    }
    public void ageing() {
        this.age++;
    }

    public int getHunger() {
        return hunger;
    }
    public String getName() {
        return name;
    }
    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public abstract void print();

    @Override
    public void feed(int food) {
        hunger -= food;
    }
    @Override
    public void hunger() {
        hunger = hunger + hungerValue;
    }

}
