package main;

import animals.Animal;
import farm.Farm;
import animals.Chicken;
import animals.Cow;
import animals.Sheep;

import java.util.Scanner;

import static animals.HungerManager.food;

public class Main {
    public static Scanner sca;
    static Farm farm;
    static int hunger;

    public static void main(String[] args) {

        // Creating a new Farm Object named farm
        farm = new Farm();

        Sheep sheep = new Sheep("Kari", 50, 5, 2);
        Cow cow = new Cow("Jonathan", 60, 7, 3);
        Chicken chicken = new Chicken("Henrietta", 60, 2, 2);

        // Adding the stardard animal from each kind to the list of all existing animals
        farm.addToAnimalList(sheep);
        farm.addToAnimalList(cow);
        farm.addToAnimalList(chicken);

        // Initialising and deklaring the loopcount
        int loopCount = 0;

        while (true) {
            loopCount++;
            // Every 3rd Round all existing animals age
            if (loopCount % 3 == 0) {
                for (Animal animal : farm.animalList) {
                    animal.ageing();
                }
                // Game-ending event after 7 rounds.
            } else if (loopCount == 7) {
                System.out.println("Eine eierlegende Wollmilchsau erscheint!");
                System.out.println("Was tust du?");
                sca = new Scanner(System.in);
                // Input without any impact
                String noImpact = sca.nextLine();
                System.out.println("Die Wollmilchsau fackelt einfach deine Farm ab!");
                break;
            }

            // Welcome message and decision making if a new or an existing animal should be chosen
            farm.startingDecision();
            // All hunger values below 0 are not allowed, and changed to 0
            for (Animal animal : farm.animalList) {
                if (animal.getHunger() < 0) {
                    animal.setHunger(0);
                    // Empty threat
                    System.out.println("Dein Tier " + animal.getName() + " ist von Adipositas bedroht.");
                }
            }
            // Print of all Animalnames and the corresponding hunger values.
            farm.printHunger();
            // Decision which animal kind should be choses for the round
            farm.animalChoice();
            // Option to choose a second animal for the round
            farm.additionalAnimal();
            // Print of all animalnames and the corresponding hunger values.
            farm.printNames();
            // Each animal selected to get fed in a round, should lose 30 points of hunger
            farm.feedingDecision(hunger);
            // Every animal gets hungrier
            farm.hungerGain();
            // Remove animal selection before a possible new round starts
            farm.removeFromSelection();

            // If any animal reaches hunger >= 100, the game ends
            boolean isSomeOneDead = false;
            for (Animal aliveCheckAnimal : farm.animalList) {
                if (aliveCheckAnimal.getHunger() >= 100) {
                    isSomeOneDead = true;
                }
            }
            if (isSomeOneDead) {
                System.out.println("Mindestens eines deiner Tiere ist leider verhungert. RiP.");
                break;
            }
        }
    }
}
