package main;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//  Aufgabe 1
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++){
                System.out.print(i + " ");
            }
            System.out.println();
        }

//  Aufgabe 2
        for (int i = 1; i <= 5; i++) {
            for (int k = 1; k <= i; k++){
                System.out.print(i + " ");
            }
            System.out.println();
        }

//  Aufgabe 3
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5-i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++){
                System.out.print(i + " ");
            }
            System.out.println();
        }
//  Aufgabe 4
        Scanner sca = new Scanner(System.in);
        System.out.print("Choose your Number of Rows:");
        int rows = sca.nextInt();
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= 9-i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++){
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
